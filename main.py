import time

import re
from selenium.webdriver import ActionChains
from selenium import webdriver

if __name__ == '__main__':
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')  # 给予root执行权限
    chrome_options.add_argument('--headless')  # 隐藏浏览器运行
    driver = webdriver.Chrome(options=chrome_options)
    url = 'https://www.shanghairanking.cn/rankings/bcur/2022'
    driver.get(url)
    time.sleep(1)
    action = ActionChains(driver)
    # 初始化界面元素 可以发现直接进入 第一页 与 后面页的元素格式不一样 但如果进入第一页后点击进入第二页
    # 再点击进入第一页，第一页就和后面的页元素一致了
    e = driver.find_element('xpath', '//*[@id="content-box"]/ul/li[9]/a')
    action.click(e).perform()
    e = driver.find_element('xpath', '//*[@id="content-box"]/ul/li[1]/a')
    action.click(e).perform()
    # 最终存储校园排行榜数据列表
    list_information = []
    # 20 表示总页数
    for j in range(0, 20):
        text = driver.page_source
        # print(text)
        list_yemian = re.findall('<tbody data-v-3fe7d390="">(.*?)</tbody>', text, re.S)[0]
        list_total = re.findall('<tr data-v-3fe7d390="">(.*?)</tr>', list_yemian, re.S)
        length = len(list_total)
        for i in range(0, length):
            list0 = list_total[i]
            # 大学中文名
            name_cn = re.findall('class="name-cn">(.*?) </a>', list0)
            # 大学英文名
            name_en = re.findall('class="name-en">(.*?) </a>', list0)
            # 大学级别
            tags = re.findall('class="tags">(.*?)</p>', list0)
            # 无级别 比如不是 985/211的学校
            # 空列表==false
            if not tags:
                tags = ['无']
            # 大学所在省市 和 大学类型
            list_province_category = re.findall(
                '<td data-v-3fe7d390="" class="">\n            (.*?)\n            <!----></td>',
                list0)
            # 大学总分
            score = re.findall('<td data-v-3fe7d390="" class="">\n            (.*?)\n          </td>', list0)
            # 办学层次
            School_level = re.findall(
                '<td data-v-3fe7d390="" class="">\n                    (.*?)\n                </td>',
                list0)
            list_0 = name_cn + name_en + tags + list_province_category + score + School_level
            list_information.append(list_0)
        # print(list_information)
        if (0 <= j <= 2) or (17 <= j <= 19):
            xpath = '//*[@id="content-box"]/ul/li[9]/a'
        elif j == 3 or j == 16:
            xpath = '//*[@id="content-box"]/ul/li[10]/a'
        else:
            xpath = '//*[@id="content-box"]/ul/li[11]/a'
        e = driver.find_element('xpath', xpath)
        action.click(e).perform()
    # print(len(list_information))
    driver.quit()
    result = open('data.xls', 'w', encoding='utf-8')
    result.write('大学名称\t英文名\t大学级别\t所在省市\t大学类型\t总分\t办学层次\n')
    for m in range(0, len(list_information)):
        for n in range(0, len(list_information[m])):
            result.write(str(list_information[m][n]))
            result.write('\t')
        result.write('\n')
    result.close()
    print('数据采集完成！！')
