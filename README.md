# 中国大学排名爬虫爬取

#### 介绍
python-selenium爬虫解决python作业爬取中国大学排名榜单

#### demo.text内容
demo.text 是每页下一页按钮xpath地址切换的规律

下一页按钮 xpath 地址变更
1-3:    //*[@id="content-box"]/ul/li[9]/a
4:      // *[ @ id = "content-box"] / ul / li[10] / a
5-16:   //*[@id="content-box"]/ul/li[11]/a
17：    //*[@id="content-box"]/ul/li[10]/a
18：    //*[@id="content-box"]/ul/li[9]/a
19：    //*[@id="content-box"]/ul/li[9]/a
20：    //*[@id="content-box"]/ul/li[9]/a


j      0  1  2  ......  19
页数    1  2  3  ......  20




#### 使用说明

请见我的CSDN博客 [python作业爬取xxx大学排行](https://blog.csdn.net/m0_57979876/article/details/126951133?spm=1001.2014.3001.5501)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



#### 万能去水印，支持多个平台

![输入图片说明](https://gitee.com/Gulu_Lv/universal-watermark-removal/raw/master/gh_2a0a366062cd_430.jpg)

#### 介绍
[小程序网站](https://happy-cliff-058679700.3.azurestaticapps.net/)

支持多个平台，包括抖音 快手 小红书 微博 微视 皮皮虾 陌陌 唱吧 西瓜视频 今日头条 好看视频 全民小视频 看点视频 趣头条 全民K歌 酷狗音乐 酷我音乐 看看视频 梨视频 哔哩哔哩 网易云音乐 看点视频 QQ看点 小咖秀 看点快报 糖豆 配音秀 大众点评 懂车帝 火山 皮皮搞笑 最左 小影  等等200多个短视频平台。
